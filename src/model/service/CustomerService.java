package model.service;

import exception.CustomerException;
import model.dto.CreateCustomerDto;
import model.dto.ResponseCustomerDto;

import java.util.List;

public interface CustomerService {
    int addNewCustomer(CreateCustomerDto createCustomerDto) throws CustomerException;
    ResponseCustomerDto searchCustomerById(Integer id) throws CustomerException;
    int deleteCustomerById(Integer id) throws CustomerException;
    int updateCustomerById(Integer id) throws CustomerException;
    List<ResponseCustomerDto> getAllCustomers() throws CustomerException;
}
