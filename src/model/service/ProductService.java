package model.service;

import exception.ProductException;
import model.dto.CreateProductDto;

public interface ProductService {
    int createNewProduct(CreateProductDto createProductDto) throws ProductException;
}