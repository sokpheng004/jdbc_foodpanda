package model.service;

import exception.CustomerException;
import mapper.Mapper;
import model.dao.CustomerDao;
import model.dao.CustomerDaoImpl;
import model.dto.CreateCustomerDto;
import model.dto.ResponseCustomerDto;
import model.entity.Customer;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;


public class CustomerServiceImpl implements CustomerService{
    private final CustomerDao customerDao = new CustomerDaoImpl();
    private final Mapper mapper = new Mapper();
    @Override
    public int addNewCustomer(CreateCustomerDto createCustomerDto) {
        Customer customer  = mapper.fromCustomerDtoToCustomer(createCustomerDto);
        customer.setCreatedDate(Date.valueOf(LocalDate.now()));
        customer.setIsDeleted(false);
        customerDao.addNewCustomer(customer);
        return 1;
    }

    @Override
    public ResponseCustomerDto searchCustomerById(Integer id) throws CustomerException {
        if(customerDao.getCustomerById(id).isEmpty()){
            throw new CustomerException("Customer Not Found.");
        }
        return mapper.fromCustomerToResponseCustomerDto(customerDao.getCustomerById(id));
    }

    @Override
    public int deleteCustomerById(Integer id) throws CustomerException{
        int rowAffected = customerDao.deleteCustomerId(id);
        if(rowAffected==0){
            throw new CustomerException("Cannot delete Customer with id: " + id);
        }
        return 1;
    }

    @Override
    public int updateCustomerById(Integer id) throws CustomerException {
        return customerDao.updateCustomerById(id);
    }

    @Override
    public List<ResponseCustomerDto> getAllCustomers() throws CustomerException {
        List<Customer> customerList = customerDao.getAllCustomers();

        if(customerList.isEmpty()){
            throw new CustomerException("Cannot select all customers from database");
        }
        return customerList.stream()
                .map(mapper::fromCustomerToResponseCustomerDto)
                .toList();
    }
}
