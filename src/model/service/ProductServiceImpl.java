package model.service;

import exception.ProductException;
import mapper.Mapper;
import model.dao.ProductDao;
import model.dao.ProductDaoImpl;
import model.dto.CreateProductDto;

public class ProductServiceImpl implements ProductService{
    private final ProductDao productDao = new ProductDaoImpl();
    private final Mapper mapper = new Mapper();
    @Override
    public int createNewProduct(CreateProductDto createProductDto) throws ProductException {
        int rowAffected = productDao.addNewProduct(mapper.fromCreateProductDtoToProduct(createProductDto));
        if(rowAffected>0){
            return 1;
        }
        throw new ProductException("Cannot create new product");
    }
}
