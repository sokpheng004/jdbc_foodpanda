package model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Customer {
    private Integer id;
    private String name;
    private String email;
    private String bio;
    private String password;
    private Boolean isDeleted;
    private Date createdDate;
    // Additional methods for interacting with the database can be added here
    public Boolean isEmpty(){
        return (id==null || id.describeConstable().isEmpty()) &&
                (name==null || name.isEmpty()) &&
                (email==null || email.isEmpty()) &&
                (password==null || password.isEmpty()) &&
                (isDeleted==null || isDeleted.describeConstable().isEmpty()) &&
                (createdDate==null);
    }
}