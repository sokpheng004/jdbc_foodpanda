package model.dao;

import model.entity.Customer;

import java.util.List;

public interface CustomerDao {
    int addNewCustomer(Customer customer);
    int updateCustomerById(Integer id);
    int deleteCustomerId(Integer id);
    Customer getCustomerById(Integer id);
    List<Customer> getAllCustomers();
}
