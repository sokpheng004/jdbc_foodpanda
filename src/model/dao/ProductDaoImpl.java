package model.dao;

import model.entity.Product;
import utils.DatabaseConfig;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class ProductDaoImpl implements ProductDao{
    private final DatabaseConfig databaseConfig = new DatabaseConfig();
    @Override
    public int addNewProduct(Product product) {
        String sql = """
                INSERT INTO (product_name,product_code,is_deleted,imported_at,expired_at,product_description)
                VALUES(?,?,?,?,?,?)
                """;
        try (
                PreparedStatement preparedStatement
                         = databaseConfig.getConnectionWithPostgresql().prepareStatement(sql)
                ){
            preparedStatement.setString(1,product.getProductName());
            preparedStatement.setString(2,product.getProductCode());
            preparedStatement.setBoolean(3,product.getIsDeleted());
            preparedStatement.setDate(4,product.getImportedDate());
            preparedStatement.setDate(5,product.getExpiredDate());
            preparedStatement.setString(6,product.getProductDescription());
            return preparedStatement.executeUpdate();
        }catch (SQLException sqlException){
            System.out.println(sqlException.getMessage());
        }
        return 0;
    }

    @Override
    public int updateProductById(Integer id) {
        return 0;
    }

    @Override
    public int deleteProductById(Integer id) {
        return 0;
    }

    @Override
    public List<Product> queryAllProducts() {
        return List.of();
    }
}
