package model.dao;

import model.entity.Product;

import java.util.List;

public interface ProductDao {
    int addNewProduct(Product product);
    int updateProductById(Integer id);
    int deleteProductById(Integer id);
    //
    List<Product> queryAllProducts();
}
