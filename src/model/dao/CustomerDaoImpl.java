package model.dao;

import model.entity.Customer;
import utils.DatabaseConfig;

import javax.print.attribute.standard.ColorSupported;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CustomerDaoImpl implements CustomerDao{
    private final DatabaseConfig databaseConfig = new DatabaseConfig();
    @Override
    public int addNewCustomer(Customer customer) {
        String sql = """
                INSERT INTO "customer" (name, email, password, is_deleted, created_date, bio)
                VALUES (?,?,?,?,?,?)
                """;
        try (
                PreparedStatement preparedStatement = databaseConfig.getConnectionWithPostgresql().prepareStatement(sql);
                ){
            preparedStatement.setString(1,customer.getName());
            preparedStatement.setString(2,customer.getEmail());
            preparedStatement.setString(3,customer.getPassword());
            preparedStatement.setBoolean(4,customer.getIsDeleted());
            preparedStatement.setDate(5,customer.getCreatedDate());
            preparedStatement.setString(6,customer.getBio());
            int rowAffected = preparedStatement.executeUpdate();
            return rowAffected;
        }catch (SQLException sqlException){
            System.out.println("[!] Problem during add new Customer: "  + sqlException.getMessage());
        }
        return 0;
    }

    @Override
    public int updateCustomerById(Integer id) {
        String sql = """
                UPDATE "customer"
                SET name = 'Chansokpheng Kim'
                WHERE id = ?
                """;
        try(
                PreparedStatement preparedStatement = databaseConfig.getConnectionWithPostgresql().prepareStatement(sql);
                ){
            //search before update
            Customer customer = getCustomerById(id);
            if(customer.isEmpty()){
                return 0;
            }
            preparedStatement.setInt(1,id);
            return preparedStatement.executeUpdate();
        }catch (SQLException sqlException){
            System.out.println(sqlException.getMessage());
        }
        return 0;
    }

    @Override
    public int deleteCustomerId(Integer id) {
        String sql = """
                DELETE FROM "customer" WHERE id = ?
                """;
        try (
                PreparedStatement preparedStatement = databaseConfig.getConnectionWithPostgresql().prepareStatement(sql);
        ){
            preparedStatement.setInt(1,id);
            return preparedStatement.executeUpdate();
        }catch (SQLException sqlException){
            System.out.println("[!] Problem during get Customer by ID: " + sqlException.getMessage());
        }
        return 0;
    }

    @Override
    public Customer getCustomerById(Integer id) {
        String sql  = """
                SELECT * FROM "customer" WHERE id = ?
                """;
        try (
                PreparedStatement preparedStatement = databaseConfig.getConnectionWithPostgresql().prepareStatement(sql);
                ){
            Customer customer = new Customer();
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                customer.setId(resultSet.getInt("id"));
                customer.setName(resultSet.getString("name"));
                customer.setEmail(resultSet.getString("email"));
                customer.setBio(resultSet.getString("bio"));
                customer.setPassword(resultSet.getString("password"));
                customer.setIsDeleted(resultSet.getBoolean("is_deleted"));
                customer.setCreatedDate(resultSet.getDate("created_date"));
            }
            return customer;
        }catch (SQLException sqlException){
            System.out.println("[!] Problem during get Customer by ID: " + sqlException.getMessage());
        }
        return new Customer();
    }

    @Override
    public List<Customer> getAllCustomers() {
        String sql = """
                SELECT * FROM "customer"
                """;
        try(PreparedStatement preparedStatement = databaseConfig.getConnectionWithPostgresql().prepareStatement(sql)) {
            List<Customer> customerList = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                customerList.add(new Customer(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("email"),
                        resultSet.getString("bio"),
                        resultSet.getString("password"),
                        resultSet.getBoolean("is_deleted"),
                        resultSet.getDate("created_date")
                ));
            }
            return customerList;
        }catch (SQLException sqlException){
            System.out.println("[!] Problem during get all customers: " + sqlException.getMessage());
        }
        return new ArrayList<>();
    }
}
