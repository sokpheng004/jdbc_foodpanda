package model.dto;

import lombok.Builder;

import java.sql.Date;

@Builder
public record CreateProductDto(
        Integer id,
        String productName,
        String productCode,
        Boolean isDeleted,
        Date importeddDate,
        Date expiredDate,
        String productDescription
) {
}