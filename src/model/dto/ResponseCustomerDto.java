package model.dto;

import lombok.Builder;

import java.sql.Date;

@Builder
public record ResponseCustomerDto(
        Integer id,
        String name,
        String email,
        String bio,
        Date createdDate
) {
}
