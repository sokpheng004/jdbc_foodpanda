package view;

import controller.CustomerController;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;
import utils.TableUI;

import javax.sql.rowset.spi.SyncResolver;
import java.util.InputMismatchException;
import java.util.Scanner;

public class UI {
    private final CustomerController customerController = new CustomerController();
    private int switchOption(String message){
        int opt;
        while (true){
            try{
                System.out.print("[+] " + message + ": ");
                opt = new Scanner(System.in).nextInt();
                break;
            }catch (InputMismatchException exception){
                System.out.println("[!] Invalid input option :(");
            }
        }
        return opt;
    }

    private void option(){
        System.out.println("""
                ----------------------------------
                1. List all Products.
                2. Sign up.
                3. Already had an account - Login.
                ----------------------------------""");
    }
    private void homeThumbnail(){
        System.out.println("=".repeat(157));
        System.out.print("-".repeat(57));
        System.out.print(" Welcome to Food Panda Console Application ");
        System.out.println("-".repeat(57));
        System.out.println("=".repeat(157));

        System.out.println("-> Dashboard");
    }
    private void processOption(){
        homeThumbnail();
        while (true){
            option();
            switch (switchOption("Insert option")){
                case 0->{
                    System.out.println("[*] System closed at the moment.");
                    try{
                        Thread.sleep(200);
                    }catch (Exception ignore){}
                    System.out.println("[>_<] Bye Bye, Have a good day...");
                    System.exit(0);
                }
                case 1->{
                    String [] columnNames = {"ID","NAME","EMAIL","BIO","CREATED DATE"};
                    TableUI.getCustomerTable(customerController.getAllCustomers(),columnNames);
                }
                default -> System.out.println("[!] Invalid option :(");
            }
        }
    }
    public void userInterface(){
        processOption();
    }
}
