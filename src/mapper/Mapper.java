package mapper;

import model.dto.CreateCustomerDto;
import model.dto.CreateProductDto;
import model.dto.ResponseCustomerDto;
import model.entity.Customer;
import model.entity.Product;

public class Mapper {
    public Customer fromCustomerDtoToCustomer(CreateCustomerDto createCustomerDto){
        if(createCustomerDto ==null){
            return null;
        }
        return Customer.builder()
                .email(createCustomerDto.email())
                .name(createCustomerDto.name())
                .password(createCustomerDto.password())
                .build();
    }
    public ResponseCustomerDto fromCustomerToResponseCustomerDto(Customer customer){
        if(customer==null){
            return null;
        }
        return ResponseCustomerDto
                .builder()
                .id(customer.getId())
                .name(customer.getName())
                .email(customer.getEmail())
                .bio(customer.getBio())
                .createdDate(customer.getCreatedDate())
                .build();
    }
    public Product fromCreateProductDtoToProduct(CreateProductDto createProductDto) {
        if (createProductDto == null) {
            return null;
        }
        return Product.builder()
                .id(createProductDto.id())
                .productName(createProductDto.productName())
                .productCode(createProductDto.productCode())
                .expiredDate(createProductDto.expiredDate())
                .importedDate(createProductDto.importeddDate())
                .productDescription(createProductDto.productDescription())
                .build();
    }
}
