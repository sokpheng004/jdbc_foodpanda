package controller;

import exception.CustomerException;
import model.dto.CreateCustomerDto;
import model.dto.ResponseCustomerDto;
import model.service.CustomerService;
import model.service.CustomerServiceImpl;

import java.util.List;

public class CustomerController {
    private final CustomerService customerService = new CustomerServiceImpl();
    public void addNewCustomer(CreateCustomerDto createCustomerDto){
        try{
            if(customerService.addNewCustomer(createCustomerDto)>0){
                System.out.println("[+] Customer account has been created successfully");
            }else {
                System.out.println("[!] Failed to created Customer account.");
            }
        }catch (CustomerException customerException){
            System.out.println(customerException.getMessage());
        }
    }
    public ResponseCustomerDto searchCustomerById(Integer id){
        try{
            return customerService.searchCustomerById(id);
        }catch (CustomerException customerException){
            System.out.println("[!] " + customerException.getMessage());
        }
        return null;
    }
    public void deleteCustomer(Integer id){
        try{
            if(customerService.deleteCustomerById(id)>0){
                System.out.println("[+] Customer has been deleted successfully");
            }else {
                System.out.println("[!] Failed to deleted Customer account.");
            }
        }catch (CustomerException customerException){
            System.out.println("[!] " + customerException.getMessage());
        }
    }
    public void updateCustomer(Integer id){
        try {
            int rowAffected = customerService.updateCustomerById(id);
            if(rowAffected==0){
                System.out.println("[!] Failed to update customer.");
            }else {
                System.out.println("[+] Updated customer successfully.");
            }
        }catch (CustomerException customerException){
            System.out.println("[!] " + customerException.getMessage());
        }
    }
    public List<ResponseCustomerDto> getAllCustomers(){
        try {
            return customerService.getAllCustomers();
        }catch (CustomerException exception){
            System.out.println("[!] " + exception.getMessage());
        }
        return null;
    }
}
