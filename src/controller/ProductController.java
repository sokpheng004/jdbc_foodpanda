package controller;


import exception.ProductException;
import model.dto.CreateProductDto;
import model.service.ProductService;
import model.service.ProductServiceImpl;

public class ProductController {
    private final ProductService productService = new ProductServiceImpl();
    public void createNewProduct(CreateProductDto createProductDto){
        try{
            productService.createNewProduct(createProductDto);
        }catch (ProductException productException){
            System.out.println("[+] " + productException.getMessage());
        }
    }
}
