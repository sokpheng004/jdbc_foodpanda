package utils;

import model.dto.ResponseCustomerDto;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.util.List;


public class TableUI {
    public static void getCustomerTable(List<ResponseCustomerDto> data, String [] columnNames){
        Table table = new Table(columnNames.length, BorderStyle.UNICODE_BOX_HEAVY_BORDER, ShownBorders.ALL);

        int i=0;
        for(String n: columnNames){
            table.addCell(n,new CellStyle(CellStyle.HorizontalAlign.center),1);
            //set width
            if(i==columnNames.length){
                i--;
            }
            table.setColumnWidth(i,25,25);
            i++;
        }
//        add data
        for(ResponseCustomerDto o: data){
            table.addCell(o.id().toString(),new CellStyle(CellStyle.HorizontalAlign.center),1);
            table.addCell(o.name(),new CellStyle(CellStyle.HorizontalAlign.center),1);
            table.addCell(o.email(),new CellStyle(CellStyle.HorizontalAlign.center),1);
            table.addCell(o.bio(),new CellStyle(CellStyle.HorizontalAlign.center),1);
            table.addCell(o.createdDate().toString(),new CellStyle(CellStyle.HorizontalAlign.center),1);
        }
        System.out.println(table.render());
    }
}
