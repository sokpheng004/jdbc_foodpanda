package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConfig {
    public Connection getConnectionWithPostgresql() throws SQLException{
        try{
            return DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/food_panda_db",
                    "postgres",
                    "123"
            );
        }catch (Exception sqlException){
            throw new SQLException("Problem with connection");
        }
    }
}
